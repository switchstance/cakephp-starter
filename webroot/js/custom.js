$(function () {
    if ($('[data-toggle="tooltip"]').length > 0) {
        $('[data-toggle="tooltip"]').tooltip();
    }

    $('.delete-entity').on('click', function () {
        var delete_url = $(this).data('delete-url');
        var item_id = $(this).data('id');
        var key = $(this).data('key');
        swal({
            title: 'Are you sure?',
            text: 'Are you sure you want to delete this?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete Record',
            confirmButtonClass: 'btn btn-square btn-lg btn-primary',
            cancelButtonClass: 'btn btn-square btn-lg btn-default',
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            buttons: {
                cancel: {
                    text: 'Cancel',
                    value: null,
                    visible: false,
                    className: 'btn btn-square btn-sm btn-secondary',
                    closeModal: true,
                },
                confirm: {
                    text: 'Yes',
                    value: true,
                    visible: true,
                    className: 'btn btn-square btn-sm btn-primary',
                    closeModal: true
                }
            }
        }, function (isConfirm) {
            if (!isConfirm) return;
            $.ajax({
                url: delete_url,
                type: 'POST',
                data: {
                    id: item_id
                },
                dataType: 'html',
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.output.status === true) {
                        swal('Success!', 'The item was successfully deleted.', 'success');
                        $('.entity-row-' + key).remove();
                        console.log('entity-row-' + key);
                    } else {
                        swal('Sorry...!', 'There was a problem. Please try again.', 'error');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal('Sorry...', 'There was a problem, please try again.', 'error');
                }
            });
        });
    });

    $(document).on('change', 'input[type=color]', function () {
        this.parentNode.style.backgroundColor = this.value;
    });
});