# Switchstance CakePHP Starter 

A starter app for Cake apps.

Includes Elisyam theme and customised templates for baking CRUS quickly and easily.


## Installation

1. Clone.
2. Run `composer install`
3. Save app.default.php as app.php with correct database and email services
5. Then you probably want to create your actual project folder and move all files from this folder into that one with new Git details.
6. Then you can get started customising and building the app.

## Customise

Colours and basic app details can be found and customised in UtilComponent
