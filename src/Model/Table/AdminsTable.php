<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Admins Model
 *
 * @method \App\Model\Entity\Admin get($primaryKey, $options = [])
 * @method \App\Model\Entity\Admin newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Admin[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Admin|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Admin saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Admin patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Admin[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Admin findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdminsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('admins');
        $this->setDisplayField('admin_id');
        $this->setPrimaryKey('admin_id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('admin_id')
            ->allowEmptyString('admin_id', 'create');

        $validator
            ->scalar('admin_name')
            ->maxLength('admin_name', 255)
            ->requirePresence('admin_name', 'create')
            ->allowEmptyString('admin_name', false);

        $validator
            ->scalar('display_name')
            ->maxLength('display_name', 255)
            ->requirePresence('display_name', 'create')
            ->allowEmptyString('display_name', false);

        $validator
            ->scalar('email_address')
            ->maxLength('email_address', 255)
            ->requirePresence('email_address', 'create')
            ->allowEmptyString('email_address', false);

        $validator
            ->scalar('admin_password')
            ->maxLength('admin_password', 255)
            ->requirePresence('admin_password', 'create')
            ->allowEmptyString('admin_password', false);

        $validator
            ->scalar('password_reset_code')
            ->maxLength('password_reset_code', 255)
            ->allowEmptyString('password_reset_code');

        $validator
            ->dateTime('password_reset_expiry_date')
            ->allowEmptyDateTime('password_reset_expiry_date');

        return $validator;
    }

    /**
     * @param Query $query
     * @param array $options
     *
     * @return Query
     */
    public function findAuth(Query $query, array $options)
    {
        $query
            ->select()
            ->where(['Admins.is_deleted IS FALSE']);

        return $query;
    }
}
