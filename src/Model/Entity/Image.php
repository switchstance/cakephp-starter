<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Image Entity
 *
 * @property int $image_id
 * @property string $image_name
 * @property string $image_size
 * @property string $image_url
 * @property string $alt_text
 * @property int|null $admin_id
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Admin $admin
 * @property \App\Model\Entity\User $user
 */
class Image extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'image_name' => true,
        'image_size' => true,
        'image_url' => true,
        'alt_text' => true,
        'admin_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'admin' => true,
        'user' => true
    ];
}
