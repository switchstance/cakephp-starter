<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\Component\UtilComponent;
use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * @property UtilComponent $Util
 *
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /** @var integer $admin_id */
    protected $admin_id;

    /**
     * @throws \Exception
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Util');

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');

        $this->loadComponent('Auth', [
            'authenticate'  => [
                'Form' => [
                    'userModel' => 'Admins',
                    'fields' => [
                        'username' => 'email_address',
                        'password' => 'admin_password',
                    ],
                    'finder' => 'auth',
                ],
            ],
            'authError' => 'Please log in.',
            'loginAction'   => [
                'controller' => 'Auth',
                'action'     => 'login',
            ],
            'loginRedirect' => '/',
            'storage' => [
                'className' => 'Session',
                'key' => 'Auth.Admin',
            ],
        ]);
    }

    /**
     * @param Event $event
     *
     * @return \Cake\Http\Response|null|void
     */
    public function beforeFilter(Event $event)
    {
        $this->setUserVariables();
    }

    /**
     * @param Event $event
     *
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewBuilder()->getVars()) &&
            in_array($this->response->getType(), ['application/json', 'application/xml'])
        )
        {
            $this->set('_serialize', true);
        }
        $this->set('referer', $this->referer());
    }

    private function setUserVariables()
    {
        $admin = $this->Auth->user();
        if (!is_null($admin))
        {
            $this->admin_id = $admin['admin_id'];

            $this->set([
                'admin' => (object) $admin,
            ]);
        }
    }
}
