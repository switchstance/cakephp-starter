<?php
namespace App\Controller;

use App\Model\Entity\Admin;

/**
 * Admins Controller
 *
 * @property \App\Model\Table\AdminsTable $Admins */
class AdminsController extends AppController
{
	
	public function index()
	{
        $search_term = $this->request->getQuery('search_term');

        $where = ['is_deleted IS FALSE'];
        if ($search_term)
        {
            $where = [
                'OR' => [
                ]
            ];
        }

        $admins_query = $this->Admins->find()->where($where);

        $admins = $this->paginate($admins_query);

        $this->set([
            'admins' => $admins,
            'search_term' => $search_term,
        ]);
        $this->set('_serialize', ['admins']);
	}

    /**
    * @param null $admin_id
    *
    * @return \Cake\Http\Response|null
    */
    public function view($admin_id = null)
    {
        $admin = $this->Admins->find()
            ->where(['Admins.admin_id' => $admin_id])
            ->first();

        if (!$admin)
        {
            $this->Flash->error('The record could not be found. Please try again.');
            return $this->redirect(['action' => 'index']);
        }

        $this->set([
            'admin' => $admin        ]);
    }

    /**
    * @return \Cake\Http\Response|null
    */
	public function add()
	{
		$admin = $this->Admins->newEntity();
		if ($this->request->is('post'))
		{
            $data = $this->request->getData();
			$admin = $this->Admins->patchEntity($admin, $data);
			if ($this->Admins->save($admin))
			{
				$this->Flash->success(__('The admin has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
                $this->log($admin->getErrors());
				$this->Flash->error(__('The admin could not be saved. Please, try again.'));
			}
		}

		
        $this->set([
            'admin' => $admin,
        ]);

	}


	    /**
    * @param null admin_id
    *
    * @return \Cake\Http\Response|null
    */
	public function edit($admin_id = null)
	{
        $admin = $this->Admins->find()->where(['admin_id' => $admin_id])->first();

        if (!$admin)
        {
            $this->Flash->error('The admin could not be found. Please, try again.');
            return $this->redirect(['action' => 'index']);
        }
		 
		if ($this->request->is(['patch', 'post', 'put']))
		{
            $data = $this->request->getData();
			$admin = $this->Admins->patchEntity($admin, $data);
			if ($this->Admins->save($admin))
			{
				$this->Flash->success(__('The admin has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
                $this->log($admin->getErrors());
				$this->Flash->error(__('The admin could not be saved. Please, try again.'));
			}
		}

		
        $this->set([
            'admin' => $admin,
        ]);
        $this->set('_serialize', ['admins']);
	}

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->request->is('json'))
        {
            $admin_id = $this->request->getData('id');

            /** @var Admin $admin */
            $admin = $this->Admins->find()->where(['admin_id' => $admin_id])->first();

            if (!$admin)
            {
                $output = [
                    'status' => false,
                    'message' => __('The $Admin could not be found. Please try again.'),
                ];
            }
            else
            {
                $admin->is_deleted = true;

                if ($this->Admins->save($admin))
                {
                    $output = [
                        'status' => true,
                    ];
                }
                else
                {
                    $this->log($admin->getErrors());
                    $output = [
                        'status' => false,
                        'message' => __('There was a problem trying to delete the Admin. Please try again.'),
                        ];
                }
            }

            $this->set(compact('output'));
            $this->set('_serialise', ['output']);
        }
        else
        {
            return $this->redirect('index');
        }
    }
}
