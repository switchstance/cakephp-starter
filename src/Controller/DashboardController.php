<?php
namespace App\Controller;

use App\Controller\Component\UtilComponent;

class DashboardController extends AppController
{
    public function index()
    {
        $this->set([
            'include_breadcrumb' => false,
            'title' => sprintf(__('%s Internal Admin'), UtilComponent::APP_NAME)
        ]);
    }
}