<?php
namespace App\Controller;

use App\Controller\Component\UtilComponent;
use App\Model\Entity\Admin;
use App\Model\Table\AdminsTable;
use Cake\Chronos\Date;
use Cake\Mailer\Email;
use Cake\Routing\Router;

/**
 * @property AdminsTable $Admins
 * @property UtilComponent $Util
 *
 * Class AuthController
 * @package App\Controller
 */
class AuthController extends AppController
{

    /**
     * @throws \Exception
     */
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['forgotPassword', 'resetPassword']);
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function login()
    {
        $this->viewBuilder()->setLayout('login');
        if ($this->request->is('post'))
        {
            $admin = $this->Auth->identify();
            if ($admin)
            {
                $data = $this->request->getData();
                $this->Auth->setUser($admin);
                $this->Flash->success(sprintf(__('Hello %s! Welcome to the %s Internal Admin.'), $admin['display_name'], UtilComponent::APP_NAME));
                return $this->redirect($this->Auth->redirectUrl(isset($data['redirect']) ? $data['redirect'] : null));
            }
            else
            {
                $this->Flash->error(__('We could not find an account with those credentials. Please check your details and try again.'));
            }
        }
        $this->set([
            'title' => 'Log in'
        ]);
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function logout()
    {
        $this->request->getSession()->destroy();
        $this->Flash->success('You have successfully logged out.');
        return $this->redirect($this->Auth->logout());
    }

    public function forgotPassword()
    {
        $this->viewBuilder()->setLayout('login');

        if ($this->request->is('post'))
        {
            $data = $this->request->getData();

            if ($data['email'] == '')
            {
                $this->Flash->error(__('Please enter an email address'));
            }

            $this->loadModel('Admins');

            /** @var Admin $admin */
            $admin = $this->Admins->find()->where(['email_address' => $data['email']])->first();

            if ($admin)
            {
                $today = new Date();
                $tomorrow = $today->modify('Add 24 hours');

                $reset_password_hash = $this->Util->generateRandomString(16);
                $reset_password_expiry_date = $tomorrow->format('Y-m-d H:i:s');

                $admin->password_reset_code = $reset_password_hash;
                $admin->password_reset_expiry_date = $reset_password_expiry_date;

                $view_vars = [
                  'password_reset_link' => Router::url(['controller' => 'Auth', 'action' => 'resetPassword', $reset_password_hash], true),
                ];

                if ($this->Admins->save($admin))
                {
                    $email = new Email();

                    $error = false;

                    // Send email
                    if (!$email
                        ->setSubject(sprintf(__('Reset your password to %s'), UtilComponent::APP_NAME))
                        ->setEmailFormat('both')
                        ->setTo($data['email'])
                        ->setFrom(UtilComponent::APP_FROM_EMAIL)
                        ->setViewVars($view_vars)
                        ->setTemplate('forgot_password')
                        ->send()
                    )
                    {
                        $error = true;
                    }

                    if ($error)
                    {
                        $this->Flash->error(__('There was a problem sending the password reset e-mail. Please try again.'));
                    }
                    else
                    {
                        $this->Flash->success(__('A link to reset your password has been sent to your e-mail address. The link will expire in 24 hours.'));
                    }
                }
                else
                {
                    $this->Flash->error(__('There was a problem trying to reset your password. Please try again.'));
                }
            }
            else
            {
                $this->Flash->error(__('There isn\'t an admin with that e-mail address. Please enter a valid e-mail.'));
            }
        }

        $this->set([
            'title' => sprintf(__('Reset your password | %s'), UtilComponent::APP_NAME),
        ]);
    }

    /**
     * @param $reset_password_code
     */
    public function resetPassword($reset_password_code)
    {
        $this->loadModel('Admins');
        $this->viewBuilder()->setLayout('login');

        $error = false;

        if ($this->request->is('post'))
        {
            $data = $this->request->getData();

            $new_password = $data['new_password'];
            $confirm_password = $data['confirm_password'];

            /** @var Admin $admin */
            $admin = $this->Admins->find()->where(['password_reset_code' => $reset_password_code])->first();

            if (!$admin)
            {
                $this->Flash->error(__('There was a problem trying to find that password reset request. Please check the link and try again.'));
                $error = true;
            }
            else
            {
                if ($new_password != $confirm_password)
                {
                    $this->Flash->error(__('The passwords don\'t match. Please try again.'));
                }
                else
                {
                    $admin->admin_password = $new_password;
                    $admin->password_reset_code = $admin->password_reset_expiry_date = null;

                    if ($this->Admins->save($admin))
                    {
                        $this->Flash->success(__('Your password has been successfully changed. You can now log in.'));
                        return $this->redirect(['controller' => 'Auth', 'action' => 'login']);
                    }
                    else
                    {
                        $this->Flash->error(__('There was a problem saving your password. Please try again.'));
                    }
                }
            }
        }
        else
        {
            /** @var Admin $admin */
            $admin = $this->Admins->find()->where(['password_reset_code' => $reset_password_code])->first();

            if (!$admin)
            {
                $this->Flash->error(__('There was a problem trying to find that password reset request. Please check the link and try again.'));
                $error = true;
            }
            else
            {
                $now = new \DateTime();

                if ($admin->password_reset_expiry_date > $now)
                {
                    $this->Flash->error(__('Sorry but the password reset request has now expired. Please request a new password reset.'));
                    $error = true;
                }
            }
        }

        $this->set([
            'reset_password_code' => $reset_password_code,
            'title' => sprintf(__('Reset your password | %s'), UtilComponent::APP_NAME),
            'error' => $error,
        ]);
    }

}