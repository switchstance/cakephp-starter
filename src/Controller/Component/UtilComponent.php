<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\Locator\TableLocator;

/**
 * Class UtilComponent
 * @package App\Controller\Component
 */
class UtilComponent extends Component
{
    const APP_NAME = 'Switchstance Cake Starter';
    const APP_FROM_EMAIL = 'devs@switchstance.agency';
    const APP_LOGO = 'img/assets/ss-logo.png';
    const APP_FAVICON = 'img/assets/ss-favicon.png';

    const APP_PRIMARY_COLOUR = '#333';
    const APP_PRIMARY_HOVER_COLOUR = '#000';
    const APP_SECONDARY_COLOUR = '#EAEAEA';
    const APP_SECONDARY_HOVER_COLOUR = '#666';

    const INCLUDE_VERSION_HISTORY = true;
    const APP_VERSION = 'v.alpha-001';

    /**
     * @param $slug
     *
     * @return mixed|null|string|string[]
     */
    public function generateSlugString($slug)
    {
        // WordPress is so much better than Cake at this.
        $slug = strtolower($slug);

        $slug = strip_tags($slug);
        // Preserve escaped octets.
        $slug = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $slug);
        // Remove percent signs that are not part of an octet.
        $slug = str_replace('%', '', $slug);
        // Restore octets.
        $slug = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $slug);
        // Convert nbsp, ndash and mdash to hyphens
        $slug = str_replace( array( '%c2%a0', '%e2%80%93', '%e2%80%94' ), '-', $slug );
        // Convert nbsp, ndash and mdash HTML entities to hyphens
        $slug = str_replace( array( '&nbsp;', '&#160;', '&ndash;', '&#8211;', '&mdash;', '&#8212;' ), '-', $slug );
        // Strip these characters entirely
        $slug = str_replace( array(
            // iexcl and iquest
            '%c2%a1', '%c2%bf',
            //// angle quotes
            '%c2%ab', '%c2%bb', '%e2%80%b9', '%e2%80%ba',
            // curly quotes
            '%e2%80%98', '%e2%80%99', '%e2%80%9c', '%e2%80%9d', '%e2%80%9a', '%e2%80%9b', '%e2%80%9e', '%e2%80%9f',
            // copy, reg, deg, hellip and trade
            '%c2%a9', '%c2%ae', '%c2%b0', '%e2%80%a6', '%e2%84%a2',
            // acute accents
            '%c2%b4', '%cb%8a', '%cc%81', '%cd%81',
            // grave accent, macron, caron
            '%cc%80', '%cc%84', '%cc%8c',
        ), '', $slug );
        // Convert times to x
        $slug = str_replace( '%c3%97', 'x', $slug );
        $slug = preg_replace('/&.+?;/', '', $slug); // kill entities
        $slug = str_replace('.', '-', $slug);

        $slug = preg_replace('/[^%a-z0-9 _-]/', '', $slug);
        $slug = preg_replace('/\s+/', '-', $slug);
        $slug = preg_replace('|-+|', '-', $slug);
        $slug = trim($slug, '-');

        return $slug;
    }

    /**
     * @param $slug
     * @param $entity
     * @param $field
     *
     * @return string
     */
    public function createSlug($slug, $entity, $field = 'slug')
    {
        $slug = $this->generateSlugString($slug) . time();

        $table_locator = new TableLocator();

        $table = $table_locator->get($entity);

        $existing_slug_search = $table->find('all', [
            'conditions' => [
                'OR' => [
                    $entity . '.' . $field . ' = ' => $slug,
                    $entity . '.' . $field . ' REGEXP' => $slug . '-[0-9]',
                ]

            ],
            'order' => [$entity . '.' . $field => 'DESC']
        ]);


        if ($existing_slug_search->count() > 0)
        {
            $existing_slug = $existing_slug_search->first();
            if (preg_match('@(-[0-9])$@', $existing_slug->$field, $matches))
            {
                $last_number = (int) substr($matches[1], -1, 1);
                $slug = $slug . '-' . ($last_number + 1);
            }
            else
            {
                $slug = $slug . '-1';
            }
        }

        // Very unlikely, but just in case someone tries to make the slug "page" which is reserved
        if ($slug == 'page')
        {
            return $this->createSlug('page-1', $entity, $field);
        }

        return $slug;
    }

    /**
     * @param int    $length
     * @param string $keyspace
     *
     * @return string
     */
    public function generateRandomString($length = 8, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i)
        {
            $str .= $keyspace[mt_rand(0, $max)];
        }

        return $str;
    }
}