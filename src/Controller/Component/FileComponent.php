<?php
namespace App\Controller\Component;

use App\Model\Entity\Image;
use App\Model\Table\ImagesTable;
use Cake\Controller\Component;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @property UtilComponent $Util
 * @property Component\FlashComponent $Flash
 *
 * Class FileComponent
 * @package App\Controller\Component
 */
class FileComponent extends Component
{
    const FILE_TYPE_IMAGE = 1;
    const FILE_TYPE_VIDEO = 2;
    const FILE_TYPE_DOCUMENT = 3;
    const FILE_TYPE_OTHER = 4;
    const FILE_TYPE_AR_MODEL = 5;

    const RETURN_ID = 1;
    const RETURN_ARRAY = 2;

    public $components = ['Util', 'Flash'];

    private $allowed_extensions = [];
    private $max_file_size_mb = 10;
    private $destination_path;

    private $file_extension;
    private $file_name_root;
    private $nice_file_name_root;

    private $new_file_name;

    private $table_locator;


    /**
     * File uploader.
     *
     * If there is a database table for the file and all went well, it will return the ID of the inserted row.
     * If there is not a database table and all went well, it will return the file location.
     * If something went wrong, at least one Flash will happen and this will return false.
     *
     * @param array $file
     * @param string $destination_path
     * @param array $options
     *
     * @return bool|string|integer|array
     */
    public function uploadFile(array $file, $destination_path, $options = [])
    {
        $file_type = self::FILE_TYPE_IMAGE;

        // Override file type if required
        if (isset($options['file_type']))
        {
            $file_type = $options['file_type'];
        }

        // Set default options and load table locator
        $this
            ->setTableLocator()
            ->setDefaultAllowedExtensions($file_type)
            ->setDestinationPath($destination_path);

        // Override allowed extensions if required
        if (isset($options['allowed_extensions']))
        {
            if (count($options['allowed_extensions']) > 0)
            {
                $this->setAllowedExtensions($options['allowed_extensions']);
            }
        }

        // Override max file size if required
        if (isset($options['max_file_size_mb']))
        {
            $this->setMaxFileSizeMb($options['max_file_size_mb']);
        }

        // Check for standard PHP upload errors
        $error = $this->getUploadedFileError($file);
        if ($error)
        {
            $this->Flash->error($error);
            return false;
        }

        // Check size is allowed
        if ($file['size'] > ($this->getMaxFileSizeMb() * 1024 * 1024))
        {
            $this->Flash->error(sprintf(__('Uploaded file is too large! Max allowed file size is %dMB'), (floatval($this->getMaxFileSizeMb()))));
            return false;
        }

        // Set file details and new name
        $this
            ->setFileExtension($file)
            ->setFileNameRoot($file)
            ->setNiceFileNameRoot($file_type)
            ->setNewFileName();


        // Check if extensions are allowed
        if (!in_array($this->getFileExtension(), $this->getAllowedExtensions()))
        {
            if (count($this->getAllowedExtensions()) == 1)
            {
                $this->Flash->error(sprintf(__('Wrong type of file detected! You can only upload files with the extension %s'), $this->getFileExtension()[0]));
            }
            else
            {
                $this->Flash->error(sprintf(__('Wrong type of file detected! You can only upload files with the extensions %s'), join(', ', $this->getAllowedExtensions())));
            }

            return false;
        }

        // Upload file
        if (!file_exists(WWW_ROOT . $this->getDestinationPath()))
        {
            if (!mkdir(WWW_ROOT . $this->getDestinationPath(), 0777, true))
            {
                $this->Flash->error(sprintf(__('There was a problem creating the image directory. Please try again or contact support.')));
                return false;
            }
        }

        // Save image
        if ($file_type == self::FILE_TYPE_IMAGE)
        {
//            if ($this->getFileExtension() != 'jpg')
//            {
//                $image_tmp = false;
//
//                if ($this->getFileExtension() === 'png')
//                {
//                    $image_tmp = imagecreatefrompng($file['tmp_name']);
//                }
//                elseif ($this->getFileExtension() === 'gif')
//                {
//                    $image_tmp = imagecreatefromgif($file['tmp_name']);
//                }
//
//                if ($image_tmp === false)
//                {
//                    $this->Flash->error(__('There was a problem saving the image. Please try again or contact support. Ref IMGTMP'));
//                    return false;
//                }
//
//
//                $result = imagejpeg($image_tmp, $this->getDestinationPath() . DS . $this->getNewFileName(), 100);
//                imagedestroy($image_tmp);
//
//                if (!$result)
//                {
//                    $this->Flash->error(__('There was a problem saving the image. Please try again or contact support. Ref IMGOTHERRES'));
//                    return false;
//                }
//            }
//            else
//            {
            $result = move_uploaded_file($file['tmp_name'], WWW_ROOT . $this->getDestinationPath() . DS . $this->getNewFileName());

            if (!$result)
            {
                $this->log($result);
                $this->Flash->error(__('There was a problem saving the image. Please try again or contact support. Ref IMGJPGRES'));
                return false;
            }
//            }


            $filename = str_replace('\\', '/', $this->getDestinationPath() . '/' . $this->getNewFileName()); /*ADD YOUR FILENAME WITH PATH*/
            $exif = @exif_read_data($filename, 0, true);
            $ort = (isset($exif['Orientation']) && $exif['Orientation']) ? $exif['Orientation'] : ((isset($exif['IFD0']['Orientation']) && $exif['IFD0']['Orientation']) ? $exif['IFD0']['Orientation'] : null); /*STORES ORIENTATION FROM IMAGE */
            $ort1 = $ort;
            if (!empty($ort1))
            {
                $image = imagecreatefromjpeg($filename);
                $ort = $ort1;
                switch ($ort) {
                    case 3:
                        $image = imagerotate($image, 180, 0);
                        break;

                    case 6:
                        $image = imagerotate($image, -90, 0);
                        break;

                    case 8:
                        $image = imagerotate($image, 90, 0);
                        break;
                }
                imagejpeg($image,$filename, 90); /*IF FOUND ORIENTATION THEN ROTATE IMAGE IN PERFECT DIMENSION*/
            }

            // If image table exists, save image
            if ($this->doesImageTableExist())
            {
                $admin_id = null;
                if (isset($options['admin_id']))
                {
                    $admin_id = $options['admin_id'];
                }

                $user_id = null;
                if (isset($options['user_id']))
                {
                    $user_id = $options['user_id'];
                }

                if ($admin_id || $user_id)
                {
                    if (isset($options['return']))
                    {
                        $return = $options['return'];
                    }
                    else
                    {
                        $return = FileComponent::RETURN_ID;
                    }

                    return $this->saveImageDetails($admin_id, $user_id, $return);
                }
            }
            return str_replace('\\', '/', $this->getDestinationPath() . '/' . $this->getNewFileName());
        }

        // Save document
        elseif ($file_type == self::FILE_TYPE_DOCUMENT)
        {
            $this->Flash->error(__('There was a problem trying to upload the document, please speak to support.'));
            return false;
        }
        // Save video
        elseif ($file_type == self::FILE_TYPE_VIDEO)
        {
            $this->Flash->error(__('There was a problem trying to upload the video. Please speak to support.'));
            return false;
        }
        elseif ($file_type == self::FILE_TYPE_OTHER)
        {
            $result = move_uploaded_file($file['tmp_name'], WWW_ROOT . $this->getDestinationPath() . DS . $this->getNewFileName());

            if (!$result)
            {
                $this->Flash->error(__('There was a problem saving the file. Please try again or contact support. Ref ERRFILERES'));
                return false;
            }

            return str_replace('\\', '/', $this->getDestinationPath() . '/' . $this->getNewFileName());
        }
        elseif ($file_type == self::FILE_TYPE_AR_MODEL)
        {
            $zip = new \ZipArchive();
            if ($zip->open($file['tmp_name']) === true)
            {
                $first_entry_stat = $zip->statIndex(0);
                if ($first_entry_stat['crc'] !== 0 && substr($first_entry_stat['name'], -1) !== '/')
                {
                    $this->Flash->error(__('ZIP file doesn\'t contain model directory.'));
                    $zip->close();
                    return false;
                }


                $gltf_file_name = '';
                for ($i = 1; $i < $zip->numFiles; $i++)
                {
                    $entry_stat = $zip->statIndex($i);
                    if (strpos($entry_stat['name'], $first_entry_stat['name']) === false)
                    {
                        $this->Flash->error(__('ZIP file root must contain only model directory.'));
                        $zip->close();
                        return false;
                    }

                    if (strpos(strtolower($entry_stat['name']), '.gltf') !== false)
                    {
                        $gltf_file_name = str_replace($first_entry_stat['name'], '', $entry_stat['name']);
                    }
                }

                if (!$gltf_file_name)
                {
                    $this->Flash->error(__('ZIP archive doesn\'t contain GLTF file.'));
                    $zip->close();
                    return false;
                }

                do
                {
                    $new_model_name = $this->Util->generateRandomString(10);
                }
                while (file_exists(WWW_ROOT . $this->getDestinationPath() . DS . $new_model_name.'.zip'));

                for ($i = 0; $i < $zip->numFiles; $i++)
                {
                    $entry_stat = $zip->statIndex($i);
                    if (!$zip->renameIndex($i, str_replace($first_entry_stat['name'], $new_model_name . DS, $entry_stat['name'])))
                    {
                        $this->Flash->error(__('Error occurred while trying to reorganise ZIP file structure.'));
                        $zip->close();
                        return false;
                    }
                }

                $zip->close();

                $result = @move_uploaded_file($file['tmp_name'], WWW_ROOT . $this->getDestinationPath() . DS . $new_model_name.'.zip');

                if (!$result)
                {
                    $this->Flash->error(__('There was a problem saving the ZIP file. Please try again or contact support. Ref ERRZIPFILERES'));
                    return false;
                }


                return [
                    'ar_model_file_name' => $gltf_file_name,
                    'ar_model_zip_file_name' => $new_model_name.'.zip',
                    'ar_model_url' => $this->getDestinationPath().DS.$new_model_name.'.zip',
                    'ar_model_file_size' => $file['size']
                ];
            }
            else
            {
                $this->Flash->error(__('Cannot open uploaded ZIP file.'));
                return false;
            }
        }
        else
        {
            $this->Flash->error(__('The file could not be saved as a valid file type was not specified. Please speak to support.'));
            return false;
        }
    }

    /**
     * @param null $admin_id
     * @param null $user_id
     * @param int  $return
     *
     * @return array|bool|int
     */
    public function saveImageDetails($admin_id = null, $user_id = null, $return = self::RETURN_ID)
    {
        /** @var ImagesTable $images_table */
        $images_table = $this->getTableLocator()->get('Images');

        /** @var Image $image */
        $image = $images_table->newEntity();

        list($width, $height) = getimagesize($this->getDestinationPath() . DS . $this->getNewFileName());
        $image_size = $width . 'x' . $height;

        $image_data = [
            'image_name' => $this->getNiceFileNameRoot(),
            'image_url' => str_replace('\\', '/', $this->getDestinationPath() . '/' . $this->getNewFileName()),
            'image_size' => $image_size,
            'alt_text' => ucwords(str_replace('-', ' ', $this->getNiceFileNameRoot())),
            'admin_id' => $admin_id,
            'user_id' => $user_id
        ];

        $image = $images_table->patchEntity($image, $image_data);

        if ($images_table->save($image))
        {
            return $return === self::RETURN_ID ? $image->image_id : $image;
        }
        else
        {
            $this->log($image->getErrors());
            $this->Flash->error(__('There was a problem saving the image details. Please try again or contact support.'));
            return false;
        }
    }

    /**
     * @param $file
     *
     * @return bool|string
     */
    private function getUploadedFileError($file)
    {
        switch ($file['error'])
        {
            case 0:
                return false;
            case 1:
            case 2:
                return __('Uploaded file is too large!');
            case 3:
                return __('The uploaded file was only partially uploaded!');
            case 4:
                return __('No file was uploaded!');
            case 6:
                return __('File upload failed! Missing a temporary folder.');
            case 7:
                return __('File upload failed! Failed to write file to disk.');
            case 8:
                return __('File upload failed! Error code #8.');
            default:
                return __('Unknown error occurred while uploading file!');
        }
    }

    /**
     * @param $file_type_id
     *
     * @return $this
     */
    private function setDefaultAllowedExtensions($file_type_id) :FileComponent
    {
        switch ($file_type_id)
        {
            case self::FILE_TYPE_IMAGE :
                $this->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif', 'svg']);
                break;
            case self::FILE_TYPE_VIDEO :
                $this->setAllowedExtensions(['wmv']);
                break;
            case self::FILE_TYPE_DOCUMENT :
                $this->setAllowedExtensions(['pdf', 'doc', 'docx', 'txt']);
                break;
        }

        return $this;
    }

    /**
     * @param array $allowed_extensions
     *
     * @return $this
     */
    private function setAllowedExtensions(array $allowed_extensions) :FileComponent
    {
        $this->allowed_extensions = $allowed_extensions;
        return $this;
    }

    /**
     * @return array
     */
    private function getAllowedExtensions() :array
    {
        return $this->allowed_extensions;
    }

    /**
     * @return int
     */
    public function getMaxFileSizeMb() :int
    {
        return $this->max_file_size_mb;
    }

    /**
     * @param $max_file_size_mb
     *
     * @return $this
     */
    public function setMaxFileSizeMb($max_file_size_mb) :FileComponent
    {
        $this->max_file_size_mb = $max_file_size_mb;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileExtension() :string
    {
        return $this->file_extension;
    }

    /**
     * @param $file
     *
     * @return $this
     */
    public function setFileExtension(array $file) :FileComponent
    {
        $this->file_extension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileNameRoot() :string
    {
        return $this->file_name_root;
    }

    /**
     * @param $file
     *
     * @return $this
     */
    public function setFileNameRoot($file) :FileComponent
    {
        $this->file_name_root = substr($file['name'], 0, ((strlen($file['name']) - strlen($this->getFileExtension())) - 1));
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNiceFileNameRoot() :string
    {
        return $this->nice_file_name_root;
    }

    /**
     * @param int $file_type_id
     *
     * @return $this
     */
    public function setNiceFileNameRoot($file_type_id = self::FILE_TYPE_IMAGE) : FileComponent
    {
        $new_file_name = $this->Util->generateSlugString($this->getFileNameRoot());

        // If we have file tables, we need to check slugs
        if ($file_type_id == self::FILE_TYPE_IMAGE)
        {
            if ($this->doesImageTableExist())
            {
                $new_file_name = $this->Util->createSlug($this->getFileNameRoot(), 'Images', 'image_name');
            }
            else
            {
                $this->log('Oh.');
            }
        }

        $this->nice_file_name_root = $new_file_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewFileName() :string
    {
        return $this->new_file_name;
    }

    /**
     * @return $this
     */
    public function setNewFileName() :FileComponent
    {
        $this->new_file_name = $this->getNiceFileNameRoot() . '.' . $this->getFileExtension();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestinationPath()
    {
        return $this->destination_path;
    }

    /**
     * @param $destination_path
     *
     * @return $this
     */
    public function setDestinationPath($destination_path)
    {
        $this->destination_path = $destination_path;
        return $this;
    }

    /**
     * @return bool
     */
    private function doesImageTableExist()
    {
        $this->getTableLocator()->get('Images');
        return $this->getTableLocator()->exists('Images');
    }

    /**
     * @return TableLocator
     */
    private function getTableLocator()
    {
        return $this->table_locator;
    }

    /**
     * @return $this
     */
    private function setTableLocator()
    {
        $this->table_locator = new TableLocator();
        return $this;
    }
}