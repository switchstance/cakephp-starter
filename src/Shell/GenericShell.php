<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\Exception\MissingDatasourceConfigException;

/**
 * Class GenericShell
 * @package App\Shell
 */
class GenericShell extends Shell
{
	/**
	 * @return \Cake\Console\ConsoleOptionParser
	 */
	public function getOptionParser()
	{
		$parser = parent::getOptionParser();

		$parser->addOption('connection', [
			'short' => 'c',
			'help' => 'Name of the database connection to use. Defaults to dev server',
			'default' => 'default',
		]);

		return $parser;
	}

	/**
	 *
	 */
	public function startup()
	{
		parent::startup();
		$db_connection_name = $this->param('connection');
		if ($db_connection_name !== 'default')
		{
			ConnectionManager::drop('default');
			try
			{
				ConnectionManager::alias($db_connection_name, 'default');
			}
			catch (MissingDatasourceConfigException $e)
			{
				$this->out("<error>There is no datasource config named: " . $db_connection_name . "</error>");
				exit();
			}
		}
	}
}