<?php
namespace App\Shell;

use App\Model\Table\AdminsTable;
use App\Model\Table\UsersTable;

/**
 * @property AdminsTable $Admins
 * @property UsersTable $Users
 *
 * Class AdminsShell
 * @package App\Shell
 */
class AuthShell extends GenericShell
{
    public function addAdmin()
    {
        $this->out($this->nl(1));
        $this->out('Add an admin user!');
        $this->out($this->nl(1));

        $admin_name = $this->in('Please enter the full name of the admin user.');

        $this->out($this->nl(1));
        $admin_display_name = $this->in('Please enter the display name for the admin user.');

        $this->out($this->nl(1));
        $admin_email_address = $this->in('And now their e-mail address.');

        $this->out($this->nl(1));
        $admin_password = $this->in('And finally, a good password.');

        $this->out($this->nl(2));
        $this->out('Thanks, just a second please...');

        $this->loadModel('Admins');

        $admin = $this->Admins->newEntity();

        $admin_data = [
            'admin_name' => $admin_name,
            'display_name' => $admin_display_name,
            'email_address' => $admin_email_address,
            'admin_password' => $admin_password,
        ];

        $admin = $this->Admins->patchEntity($admin, $admin_data);

        $this->out($this->nl(2));
        if (!$this->Admins->save($admin))
        {
            $this->out('I have malfunctioned :(');
            $this->out($admin->getErrors());
        }
        else
        {
            $this->out('The admin was successfully created! :D');
            $this->out('You can now log in to the admin with these details.');
        }
    }

    public function addUser()
    {
        $this->out($this->nl(1));
        $this->out('Add an app user!');
        $this->out($this->nl(1));

        $user_name = $this->in('Please enter the full name of the app user.');

        $this->out($this->nl(1));
        $user_display_name = $this->in('Please enter the display name for the app user.');

        $this->out($this->nl(1));
        $user_email_address = $this->in('And now their e-mail address.');

        $this->out($this->nl(1));
        $user_password = $this->in('And finally, a good password.');

        $this->out($this->nl(2));
        $this->out('Thanks, just a second please...');

        $this->loadModel('Users');

        $user = $this->Users->newEntity();

        $user_data = [
            'user_name' => $user_name,
            'display_name' => $user_display_name,
            'email_address' => $user_email_address,
            'user_password' => $user_password,
            'is_live' => true
        ];

        $user = $this->Users->patchEntity($user, $user_data);

        $this->out($this->nl(2));
        if (!$this->Users->save($user))
        {
            $this->out('I have malfunctioned :(');
            $this->out($user->getErrors());
        }
        else
        {
            $this->out('The user was successfully created! :D');
            $this->out('You can now log in to the app with these details.');
        }
    }
}