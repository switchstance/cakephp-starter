<?php
namespace App\View\Helper;

use App\Controller\Component\UtilComponent;
use Cake\Log\Log;
use Cake\Network\Request;
use Cake\View\Helper;
use Psr\Log\LogLevel;

class UtilHelper extends Helper
{
    /**
     * @param $message
     */
    public function log($message)
    {
        Log::write(LogLevel::ERROR, $message);
    }

    /**
     * Return a tick or cross icon for boolean displays
     *
     * @param $true_or_false
     * @param $options
     *
     * @return string
     */
    public function getBooleanIcon($true_or_false, $options = [])
    {
        $options['icon_provider'] = isset($options['icon_provider']) ? $options['icon_provider'] : 'fa';
        $options['true_icon'] = isset($options['true_icon']) ? $options['true_icon'] : 'fa-check';
        $options['false_icon'] = isset($options['false_icon']) ? $options['false_icon'] : 'fa-times';
        $options['true_colour'] = isset($options['true_colour']) ? $options['true_colour'] : 'c-green';
        $options['false_colour'] = isset($options['false_colour']) ? $options['false_colour'] : 'c-red';
        $options['return_null_value'] = isset($options['return_null_value']) ? $options['return_null_value'] : false;

        $true_return = '<span class="' . $options['icon_provider'] . ' ' . $options['true_icon'] . ' ' . $options['true_colour'] . '"></span>';
        $false_return = '<span class="' . $options['icon_provider'] . ' ' . $options['false_icon'] . ' ' . $options['false_colour'] . '"></span>';


        if ($options['return_null_value'] !== false)
        {
            return $true_or_false === null ? $options['return_null_value'] : ($true_or_false ? $true_return : $false_return);
        }

        return $true_or_false ? $true_return : $false_return;
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public function getMetaTitle(Request $request)
    {
        $controller_name = \Cake\Utility\Inflector::humanize(
            \Cake\Utility\Inflector::tableize($request->getParam('controller'))
        );

        $method_name = $request->getParam('action') == 'index'
            ? sprintf(__('All %s'), \Cake\Utility\Inflector::humanize(
                \Cake\Utility\Inflector::tableize($request->getParam('controller'))
            ))
            : ucfirst($request->getParam('action')) . ' ' . \Cake\Utility\Inflector::humanize(
                \Cake\Utility\Inflector::singularize(
                    \Cake\Utility\Inflector::tableize($request->getParam('controller'))
                )
            );

        if ($method_name == null && $controller_name == null) :
            $title = UtilComponent::APP_NAME;
        elseif ($method_name == null && $controller_name !== null) :
            $title = $controller_name . ' | ' . UtilComponent::APP_NAME;
        else :
            $title = $method_name . ' | ' . $controller_name . ' | ' . UtilComponent::APP_NAME;
        endif;

        return $title;
    }

    /**
     * @return string
     */
    public function getCurrentUrl()
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }
}