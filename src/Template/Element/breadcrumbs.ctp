<?php
/**
 * @var string $breadcrumb_override
 * @var boolean $include_breadcrumb
 */

$include_breadcrumb = isset($include_breadcrumb) ? $include_breadcrumb : true;

if ($include_breadcrumb) :
?>

<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
    <li class="pr-2">
        <?= $this->Html->link(__('Dashboard'), ['controller' => 'Dashboard', 'action' => 'index']) ?>
        &bull;
    </li>
    <li class="pr-2">
        <?php if (isset($breadcrumb_override)) :
            echo $breadcrumb_override;
        else :
            $controller_name = \Cake\Utility\Inflector::humanize(
                \Cake\Utility\Inflector::tableize($this->request->getParam('controller'))
            ) ?>
            <?= $this->Html->link($controller_name, ['action' => 'index']) ?>
            &bull;
        <?php endif; ?>
    </li>
    <?php if (!isset($breadcrumb_override)) : ?>
        <li>
            <?php $method_name = $this->request->getParam('action') == 'index'
                ? sprintf(__('All %s'), \Cake\Utility\Inflector::humanize(
                    \Cake\Utility\Inflector::tableize($this->request->getParam('controller'))
                ))
                : ucfirst($this->request->getParam('action')) . ' ' . \Cake\Utility\Inflector::humanize(
                    \Cake\Utility\Inflector::singularize(
                        \Cake\Utility\Inflector::tableize($this->request->getParam('controller'))
                    )
                ); ?>
            <span><?= $method_name ?></span>
        </li>
    <?php endif; ?>
</ul>
<?php endif;