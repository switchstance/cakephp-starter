<?php
use App\Controller\Component\UtilComponent;
?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= isset($title) ? $title : $this->Util->getMetaTitle($this->request) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <style type="text/css">
        :root {
            --primary-color: <?= UtilComponent::APP_PRIMARY_COLOUR ?>;
            --primary-color-hover: <?= UtilComponent::APP_PRIMARY_HOVER_COLOUR ?>;
            --secondary-color: <?= UtilComponent::APP_SECONDARY_COLOUR ?>;
            --secondary-color-hover: <?= UtilComponent::APP_SECONDARY_HOVER_COLOUR ?>;
        }
    </style>

    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/elisyam/css/base/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/elisyam/css/base/elisyam-1.5.min.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/elisyam/css/datatables/datatables.min.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/bootstrap-sweetalert/sweetalert.css">

    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>css/custom.css">

    <link href="https://fonts.googleapis.com/css?family=Noto+Serif|Work+Sans&display=swap" rel="stylesheet">

    <link rel="icon" href="<?= $this->Url->build('/', true) . UtilComponent::APP_FAVICON ?>">

    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css"
          integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">

</head>