<?php
/**
 * @var string $password_reset_link
 */
?>

<p>You have requested a password reset.</p>
<p>Please click the link below or copy and paste it into your web browser.</p>
<p><a href="<?= $password_reset_link ?>"><?= $password_reset_link ?></a></p>
<p>This link will expire in 24 hours.</p>
<p>If you didn't request a password reset, please ignore this e-mail.</p>
<p><small>This e-mail address isn't monitored so please don't reply.</small></p>