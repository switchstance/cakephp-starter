<div class="horizontal-menu">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-light navbar-expand-lg main-menu ">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li><a href="<?= $this->Url->build(['controller' => 'Dashboard', 'action' => 'index']) ?>"><?= __('Dashboard') ?></a></li>
                        <li></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" id="dashboard" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= __('Config') ?></a>
                            <ul class="dropdown-menu" aria-labelledby="dashboard">
                                <li>
                                    <a href="<?= $this->Url->build(['controller' => 'Admins', 'action' => 'index']) ?>">
                                        <?= __('Admins') ?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>