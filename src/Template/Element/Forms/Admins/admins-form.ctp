<?php
/**
 * @var \App\View\AppView $this
 * @var string $method
 * @var App\Model\Entity\Admin $admin */
?>

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2><?= $method == 'add' ? __('Add New Admin') : __('Edit Admin') ?></h2>
            </div>
            <div class="widget-body">
                <?= $this->Form->create($admin); ?>

                <?= $this->Form->control('admin_id') ?>
                <?= $this->Form->control('admin_name', ['label' => __('Full Name')]) ?>
                <?= $this->Form->control('display_name') ?>
                <?= $this->Form->control('email_address') ?>
                <?php if ($method === 'add') : ?>
                <?= $this->Form->control('admin_password', [
                        'label' => __('Password'),
                        'type' => 'password',
                    ]) ?>
                <?php endif; ?>

                <div class="text-right">
                    <input class="btn btn-primary btn-square" type="submit" value="<?= __('Save Record') ?>">
                </div>

                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>