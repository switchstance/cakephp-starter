<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Admin $admin
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Admin'), ['action' => 'edit', $admin->admin_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Admin'), ['action' => 'delete', $admin->admin_id], ['confirm' => __('Are you sure you want to delete # {0}?', $admin->admin_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Admins'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Admin'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="admins view large-9 medium-8 columns content">
    <h3><?= h($admin->admin_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Admin Name') ?></th>
            <td><?= h($admin->admin_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Display Name') ?></th>
            <td><?= h($admin->display_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email Address') ?></th>
            <td><?= h($admin->email_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Admin Password') ?></th>
            <td><?= h($admin->admin_password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password Reset Code') ?></th>
            <td><?= h($admin->password_reset_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Admin Id') ?></th>
            <td><?= $this->Number->format($admin->admin_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password Reset Expiry Date') ?></th>
            <td><?= h($admin->password_reset_expiry_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($admin->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($admin->modified) ?></td>
        </tr>
    </table>
</div>
