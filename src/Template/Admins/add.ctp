
<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title"><?= __('Admins') ?></h2>
        </div>
    </div>
</div>
<!-- End Page Header -->

<?= $this->element('Forms'.DS.'Admins'.DS.'admins-form', ['method' => 'add'])  ?>