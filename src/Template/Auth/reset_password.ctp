<?php
use App\Controller\Component\UtilComponent;
/**
 * @var boolean $error
 * @var string $reset_password_code
 * @var \App\View\AppView $this
 */
?>

<div class="container-fluid h-100 overflow-y">
    <div class="row flex-row h-100">
        <div class="col-12 my-auto">
            <div class="password-form mx-auto">
                <div class="logo-centered">
                    <a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'login']) ?>">
                        <img src="<?= $this->Url->build('/', true) . UtilComponent::APP_LOGO ?>" alt="<?= UtilComponent::APP_NAME ?>">
                    </a>
                </div>
                <?= $this->Flash->render() ?>
                <?php if (!$error) : ?>
                    <h3><?= __('Reset Password') ?></h3>
                    <form method="post" action="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'reset-password', $reset_password_code]) ?>">
                        <div class="group material-input">
                            <input type="password" required="required" id="new_password" name="new_password">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label for="new_password"><?= __('New password') ?></label>
                        </div>
                        <div class="group material-input">
                            <input type="password" required="required" id="confirm_password" name="confirm_password">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label for="confirm_password"><?= __('Confirm password') ?></label>
                        </div>
                        <div class="button text-center">
                            <input type="submit" value="<?= __('Reset Password') ?>" class="btn btn-primary-blue btn-square">
                        </div>
                    </form>
                <?php endif; ?>
                <div class="back">
                    <a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'login']) ?>"><?= __('Return to log in') ?></a>
                </div>
            </div>
        </div>

    </div>

</div>