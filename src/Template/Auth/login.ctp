<?php
use App\Controller\Component\UtilComponent;
?>

<div class="container-fluid h-100 overflow-y">
    <div class="row flex-row h-100">
        <div class="col-12 my-auto">
            <div class="password-form mx-auto">
                <div class="logo-centered">
                    <img src="<?= $this->Url->build('/', true) . UtilComponent::APP_LOGO ?>" class="logo-big">
                </div>
                <?= $this->Flash->render() ?>
                <h3><?= __('Sign in') ?></h3>
                <?= $this->Form->create(); ?>
                <div class="group material-input">
                    <input type="text" required="required" name="email_address" id="email_address">
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label for="email_address"><?= __('Email') ?></label>
                </div>
                <div class="group material-input">
                    <input type="password" required="required" name="admin_password" id="admin_password">
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label for="admin_password"><?= __('Password') ?></label>
                </div>


                <div class="sign-btn text-center">
                    <input type="submit" value="<?= __('Sign in') ?>" class="btn btn-primary btn-square">
                </div>
                <?= $this->Form->end() ?>
                <div class="back">
                    <a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'forgotPassword']) ?>"><?= __('Forgot Password?') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>