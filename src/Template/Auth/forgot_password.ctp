<?php
use App\Controller\Component\UtilComponent;
/**
* @var \App\View\AppView $this
*/
?>

<div class="container-fluid h-100 overflow-y">
    <div class="row flex-row h-100">
        <div class="col-12 my-auto">
            <div class="password-form mx-auto">
                <div class="logo-centered">
                    <a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'login']) ?>">
                        <img src="<?= $this->Url->build('/', true) . UtilComponent::APP_LOGO ?>" alt="<?= UtilComponent::APP_NAME ?>" class="logo-big">
                    </a>
                </div>
                <?= $this->Flash->render() ?>
                <h3><?= __('Reset Password') ?></h3>
                <form method="post" action="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'forgot-password']) ?>">
                    <div class="group material-input">
                        <input type="email" required="required" id="email" name="email">
                        <span class="highlight"></span>
                        <span class="bar"></span>
                        <label for="email"><?= __('Email') ?></label>
                    </div>
                    <div class="button text-center">
                        <input type="submit" value="<?= __('Reset Password') ?>" class="btn btn-primary-blue btn-square">
                    </div>
                </form>
                <div class="back">
                    <a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'login']) ?>"><?= __('Return to log in') ?></a>
                </div>
            </div>
        </div>

    </div>

</div>