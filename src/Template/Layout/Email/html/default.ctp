<?php
use Cake\Routing\Router;
use App\Controller\Component\UtilComponent;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <title><?= $this->fetch('title') ?></title>
    <style type="text/css">
        p {
            font-size: 14px;
            line-height: 120%;
            padding: 0 0 15px 0;
            margin: 0;
            color: #333;
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body style="background: #f4f4f4;">
<div style="padding: 30px 0;">
    <div style="width: 600px; margin: 0 auto; background: #fff; border: 1px solid #eaeaea;">
        <div style="padding: 10px 0; text-align: center;">
            <img src="<?= Router::url('/', true) . UtilComponent::APP_LOGO ?>" alt="<?= UtilComponent::APP_NAME ?>">
        </div>
        <div style="padding: 10px">
            <?= $this->fetch('content') ?>
        </div>
    </div>
</div>
</body>
</html>
