<?php
use App\Controller\Component\UtilComponent;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Admin $admin
 */
?>
<!DOCTYPE html>
<html lang="en">
<?= $this->element('Layout/head') ?>
<body>
<div class="page db-modern">
    <!-- Begin Header -->
    <header class="header">
        <div class="container">
            <nav class="navbar" style="padding: 10px 0;">
                <!-- Begin Topbar -->
                <div class="navbar-holder d-flex align-items-center align-middle justify-content-between">
                    <!-- Begin Logo -->
                    <div class="navbar-header">
                        <a href="<?= $this->Url->build(['controller' => 'Dashboard', 'method' => 'index']) ?>" class="navbar-brand">
                            <div class="brand-image brand-big">
                                <img src="<?= $this->Url->build('/', true) . UtilComponent::APP_LOGO ?>" alt="<?= UtilComponent::APP_NAME ?>" style="max-height:50px;">
                            </div>
                            <div class="brand-image brand-small">
                                <img src="<?= $this->Url->build('/', true) . UtilComponent::APP_LOGO ?>" alt="<?= UtilComponent::APP_LOGO ?>"
                                     class="logo-small">
                            </div>
                        </a>
                    </div>
                    <!-- End Logo -->
                    <!-- Begin Navbar Menu -->
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center pull-right">
                        <li class="pr-3">
                            <?= sprintf(__('Welcome back %s!'), $admin->display_name) ?>
                        </li>
                        <li class="pr-3">&bull;</li>
                        <li class="pr-3">
                            <a href="<?= $this->Url->build(['controller' => 'Admins', 'action' => 'edit', $admin->admin_id]) ?>" class="a-primary">
                                <?= __('Edit Profile') ?>
                            </a>
                        </li>
                        <li class="pr-3">&bull;</li>
                        <li>
                            <a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'logout']) ?>">
                                <?= __('Log out') ?> <span class="fas fa-sign-out-alt"></span>
                            </a>
                        </li>
                    </ul>
                    <!-- End Navbar Menu -->
                </div>
                <!-- End Topbar -->
            </nav>
        </div>
    </header>
    <!-- End Header -->
    <!-- Begin Page Content -->
    <div class="page-content">
        <?= $this->element('menu'); ?>
        <div class="content-inner boxed mt-4 w-100">

            <div class="container clearfix">
                <?= $this->element('breadcrumbs') ?>
                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>
            </div>
            <!-- End Container -->
            <!-- Begin Page Footer-->
            <footer class="main-footer fixed-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 d-flex align-items-center justify-content-xl-start justify-content-lg-start justify-content-md-start justify-content-center">
                            <p class="text-gradient-02"><?= sprintf(__('%s &bull; Development by Switchstance %d.'), UtilComponent::APP_NAME, date('Y')) ?>
                                <?= UtilComponent::INCLUDE_VERSION_HISTORY ? sprintf(
                                        __(' &bull; %s &bull; <a href="%s">Version History</a>'),
                                        UtilComponent::APP_VERSION, $this->Url->build(['controller' => 'Pages', 'action' => 'display', 'version-history'])
                                    ) : '' ?></p>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Page Footer -->
            <a href="#" class="go-top"><i class="la la-arrow-up"></i></a>

        </div>
        <!-- End Offsidebar Container -->
    </div>
    <!-- End Offcanvas Sidebar -->

    <div id="loader-modal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="progress progress-lg mt-5">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="section-title mt-3 mb-5">
                        <h2>Loading Data..</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?= $this->element('Layout/foot') ?>
</body>
</html>