
	<%
	$belongsTo = $this->Bake->aliasExtractor($modelObj, 'BelongsTo');
	$belongsToMany = $this->Bake->aliasExtractor($modelObj, 'BelongsToMany');
	$compact = ["'" . $singularName . "'"];
	%>
    /**
    * @param null <%= $singularName %>_id
    *
    * @return \Cake\Http\Response|null
    */
	public function edit($<%= $singularName %>_id = null)
	{
        $<%= $singularName %> = $this-><%= $currentModelName %>->find()->where(['<%= $singularName %>_id' => $<%= $singularName %>_id])->first();

        if (!$<%= $singularName %>)
        {
            $this->Flash->error('The <%= $singularName %> could not be found. Please, try again.');
            return $this->redirect(['action' => 'index']);
        }
		 
		if ($this->request->is(['patch', 'post', 'put']))
		{
            $data = $this->request->getData();
			$<%= $singularName %> = $this-><%= $currentModelName %>->patchEntity($<%= $singularName %>, $data);
			if ($this-><%= $currentModelName %>->save($<%= $singularName %>))
			{
				$this->Flash->success(__('The <%= strtolower($singularHumanName) %> has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
                $this->log($<%= $singularName %>->getErrors());
				$this->Flash->error(__('The <%= strtolower($singularHumanName) %> could not be saved. Please, try again.'));
			}
		}

		<%
		foreach (array_merge($belongsTo, $belongsToMany) as $assoc):
		$association = $modelObj->association($assoc);
		$otherName = $association->target()->alias();
		$otherPlural = $this->_variableName($otherName);
		%>
		$<%= $otherPlural %> = $this-><%= $currentModelName %>-><%= $otherName %>->find('list', ['limit' => 200]);
		<%
		$compact[] = "'$otherPlural'";
		endforeach;
		%>

        $this->set([
            '<%= $singularName %>' => $<%= $singularName %>,
        ]);
        $this->set('_serialize', ['<%= $pluralName %>']);
	}

