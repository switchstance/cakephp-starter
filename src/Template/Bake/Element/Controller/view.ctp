
    /**
    * @param null $<%= $singularName %>_id
    *
    * @return \Cake\Http\Response|null
    */
    public function view($<%= $singularName %>_id = null)
    {
        $<%= $singularName %> = $this-><%= $currentModelName %>->find()
            ->where(['<%= $currentModelName %>.<%= $singularName %>_id' => $<%= $singularName %>_id])
            ->first();

        if (!$<%= $singularName %>)
        {
            $this->Flash->error('The record could not be found. Please try again.');
            return $this->redirect(['action' => 'index']);
        }

        $this->set([
            '<%= strtolower($singularHumanName) %>' => $<%= $singularName %>
        ]);
    }
