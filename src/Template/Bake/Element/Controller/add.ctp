<%
$compact = ["'" . $singularName . "'"];
%>

    /**
    * @return \Cake\Http\Response|null
    */
	public function add()
	{
		$<%= $singularName %> = $this-><%= $currentModelName %>->newEntity();
		if ($this->request->is('post'))
		{
            $data = $this->request->getData();
			$<%= $singularName %> = $this-><%= $currentModelName %>->patchEntity($<%= $singularName %>, $data);
			if ($this-><%= $currentModelName %>->save($<%= $singularName %>))
			{
				$this->Flash->success(__('The <%= strtolower($singularHumanName) %> has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
                $this->log($<%= $singularName %>->getErrors());
				$this->Flash->error(__('The <%= strtolower($singularHumanName) %> could not be saved. Please, try again.'));
			}
		}

		<%
		$associations = array_merge(
		$this->Bake->aliasExtractor($modelObj, 'BelongsTo'),
		$this->Bake->aliasExtractor($modelObj, 'BelongsToMany')
		);
		foreach ($associations as $assoc):
		$association = $modelObj->association($assoc);
		$otherName = $association->target()->alias();
		$otherPlural = $this->_variableName($otherName);
		%>
		$<%= $otherPlural %> = $this-><%= $currentModelName %>-><%= $otherName %>->find('list', ['limit' => 200]);
		<%
		$compact[] = "'$otherPlural'";
		endforeach;
		%>

        $this->set([
            '<%= $singularName %>' => $<%= $singularName %>,
        ]);

	}

