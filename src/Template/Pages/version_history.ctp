<div class="widget widget-02 has-shadow">
    <div class="widget-header">
        <h1><?= __('Version History') ?></h1>
    </div>
    <div class="widget-body">
        <div class="row">
            <div class="col-md-12">

                <!-- ADD SEPARATOR BETWEEN COMMITS
                <div class="em-separator separator-dashed"></div>
                -->

                <h2>v.alpha-001 <small class="text-grey-light">DD/MM/YYYY</small></h2>
                <ul>
                    <li> &bull; Initial commit and set-up of framework.</li>
                    <li> &bull; Form and pagination template set up.</li>
                    <li> &bull; Admins CRUD, admin shell task set up.</li>
                    <li> &bull; Auth (log in, reset password)</li>
                </ul>
            </div>
        </div>
    </div>
</div>