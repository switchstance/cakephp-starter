<?php
use Migrations\AbstractMigration;

class AuthMigrations1 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users', ['id' => false, 'primary_key' => 'user_id']);
        $table
            ->addColumn('user_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('user_name', 'string', ['limit' => 255])
            ->addColumn('display_name', 'string', ['limit' => 255])
            ->addColumn('email_address', 'string', ['limit' => 255])
            ->addColumn('user_password', 'string', ['limit' => 255])
            ->addColumn('fcm_token', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('password_reset_code', 'string', ['limit' => 32, 'null' => true])
            ->addColumn('password_reset_code_expiration_date', 'datetime', ['null' => true])
            ->addColumn('is_live', 'boolean', ['default' => false])
            ->addColumn('is_deleted', 'boolean', ['default' => false])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();

        $admins_table = $this->table('admins', ['id' => false, 'primary_key' => 'admin_id']);
        $admins_table
            ->addColumn('admin_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('admin_name', 'string', ['limit' => 255])
            ->addColumn('display_name', 'string', ['limit' => 255])
            ->addColumn('email_address', 'string', ['limit' => 255])
            ->addColumn('admin_password', 'string', ['limit' => 255])
            ->addColumn('password_reset_code', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('password_reset_expiry_date', 'datetime', ['null' => true])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();
    }
}
