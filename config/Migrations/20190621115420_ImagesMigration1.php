<?php
use Migrations\AbstractMigration;

class ImagesMigration1 extends AbstractMigration
{
    public function change()
    {
        $images_table = $this->table('images', ['id' => false, 'primary_key' => 'image_id']);
        $images_table
            ->addColumn('image_id', 'integer', ['signed' => false, 'limit' => 11, 'identity' => true])
            ->addColumn('image_name', 'string', ['limit' => 255])
            ->addColumn('image_size', 'string', ['limit' => 255])
            ->addColumn('image_url', 'string', ['limit' => 255])
            ->addColumn('alt_text', 'string', ['limit' => 255])
            ->addColumn('admin_id', 'integer', ['limit' => 11, 'signed' => false, 'null' => true])
            ->addForeignKey('admin_id', 'admins', 'admin_id', ['update' => 'CASCADE', 'delete' => 'RESTRICT'])
            ->addColumn('user_id', 'integer', ['limit' => 11, 'signed' => false, 'null' => true])
            ->addForeignKey('user_id', 'users', 'user_id', ['update' => 'CASCADE', 'delete' => 'RESTRICT'])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();
    }
}
