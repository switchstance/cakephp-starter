<?php
use Migrations\AbstractMigration;

class AuthMigration2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $admins_table = $this->table('admins');
        $admins_table
            ->addColumn('is_deleted', 'boolean', ['default' => false, 'after' => 'password_reset_expiry_date'])
            ->update();
    }
}
