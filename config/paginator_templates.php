<?php
return [
    'number' => '<a href="{{url}}" class="page-link">{{text}}</a>',
    'current' => '<li class="active"><a href="#" class="page-link">{{text}}</a></li>',
    'nextActive' => '<li><a aria-label="Next" href="{{url}}" class="page-link"><span aria-hidden="true" class="fa fa-angle-right"></span></a></li>',
    'nextDisabled' => '<li class="next disabled"><a aria-label="Next" class="page-link"><span aria-hidden="true" class="fa fa-angle-right"></span></a></li>',
    'prevActive' => '<li><a aria-label="Previous" href="{{url}}" class="page-link"><span aria-hidden="true" class="fa fa-angle-left"></span></a></li>',
    'prevDisabled' => '<li class="prev disabled"><a aria-label="Previous" class="page-link"><span aria-hidden="true" class="fa fa-angle-left"></span></a></li>',
    'last' => '<li class="last"><a href="{{url}}" class="page-link"><span class="fa fa-angle-double-right"></span></a></li>',
    'first' => '<li class="first"><a href="{{url}}" class="page-link"><span class="fa fa-angle-double-left"></span></a></li>'
];