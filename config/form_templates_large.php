<?php
return [
    'formStart' => '<form class="form-horizontal" {{attrs}}><div class="form-body">',
    'formEnd' => '</div></form>',
    'file' => '<div class="col-md-8"><input type="file" name="{{name}}"{{attrs}}></div>',
    'label' => '<label class="col-lg-12 form-control-label d-flex" {{attrs}}>{{text}}</label>',
    'hidden' => '<div class="col-md-12"><input class="form-control" type="hidden" name="{{name}}" {{attrs}} /></div>',
    'input' => '<div class="col-lg-8 col-sm-8 offset-sm-2"><input class="form-control {{name}}" type="{{type}}" name="{{name}}" {{attrs}} /></div>',
    'select' => '<div class="col-md-8 col-sm-8 offset-sm-2">HELLO<select class="form-control" name="{{name}}"{{attrs}}>{{content}}</select></div>',
    'textarea' => '<div class="col-md-8 col-sm-8 offset-sm-2"><textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea></div>',
    'inputContainer' => '<div class="form-group row d-flex align-items-center mb-5 {{type}}{{required}}" form-type="{{type}}">{{content}}</div>',
    'inputContainerError' => '<div class="form-group row d-flex align-items-center has-error {{type}}{{required}}" form-type="{{type}}">{{content}}</div><div class="form-group row d-flex align-items-center mb-5"><div class="col-md-6 offset-md-4"><span class="help-block">{{error}}</span></div></div>',
    'checkbox' => '<input type="checkbox" name="{{name}}"{{attrs}}>',
    'button' => '<div class="form-actions"><div class="row">'
        . '<div class="col-md-offset-3"><button class="btn green" {{attrs}}>{{text}}</button>'
        . '</div></div></div>',
    'error' => '<div class="error-message">{{content}}</div>',
    'nestingLabel' => '<label class="mt-checkbox mt-checkbox-outline col-md-offset-3"'
        . '{{attrs}}>{{hidden}}{{input}}<span>{{text}}</span></label>',
]
    ;