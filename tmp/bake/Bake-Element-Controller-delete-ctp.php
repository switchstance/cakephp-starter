    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->request->is('json'))
        {
            $<?= $singularName ?>_id = $this->request->getData('id');

            /** @var <?= ucfirst($singularName) ?> $<?= $singularName ?> */
            $<?= $singularName ?> = $this-><?= $currentModelName ?>->find()->where(['<?= $singularName ?>_id' => $<?= $singularName ?>_id])->first();

            if (!$<?= $singularName ?>)
            {
                $output = [
                    'status' => false,
                    'message' => __('The $<?= $singularHumanName ?> could not be found. Please try again.'),
                ];
            }
            else
            {
                $<?= $singularName ?>->is_deleted = true;

                if ($this-><?= $currentModelName ?>->save($<?= $singularName ?>))
                {
                    $output = [
                        'status' => true,
                    ];
                }
                else
                {
                    $this->log($<?= $singularName ?>->getErrors());
                    $output = [
                        'status' => false,
                        'message' => __('There was a problem trying to delete the <?= $singularHumanName ?>. Please try again.'),
                        ];
                }
            }

            $this->set(compact('output'));
            $this->set('_serialise', ['output']);
        }
        else
        {
            return $this->redirect('index');
        }
    }