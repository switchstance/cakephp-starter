<?php
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @since         0.1.0
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/
?>
<CakePHPBakeOpenTagphp
/**
 * @var \<?= $namespace ?>\View\AppView $this
 * @var <?= $entityClass ?>[]|\Cake\Collection\CollectionInterface $<?= $pluralVar ?>
 */
CakePHPBakeCloseTag>
<?php
use Cake\Utility\Inflector;

$fields = collection($fields)
->filter(function($field) use ($schema) {
return !in_array($schema->columnType($field), ['binary', 'text']);
});

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
$fields = $fields->reject(function ($field) {
return $field === 'lft' || $field === 'rght';
});
}

if (!empty($indexColumns)) {
$fields = $fields->take($indexColumns);
}

?>

<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title"><CakePHPBakeOpenTag= __('<?= $pluralHumanName ?>') CakePHPBakeCloseTag></h2>
            <div>
                <a class="btn btn-square btn-outline-primary" href="<CakePHPBakeOpenTag= $this->Url->build(['controller' => '<?= $pluralHumanName ?>', 'action' => 'add']) CakePHPBakeCloseTag>">
                    <CakePHPBakeOpenTag= __('Add New <?= $singularHumanName ?>') CakePHPBakeCloseTag>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->


<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <!--
            <div class="widget-header bordered no-actions d-flex align-items-center">
            </div>
            -->
            <div class="widget-body">
                <CakePHPBakeOpenTagphp if (count($<?= $pluralVar ?>) > 0) : CakePHPBakeCloseTag>
                <div class="table-responsive">
                    <div id="sorting-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div id="sorting-table_filter" class="dataTables_filter">
                                    <form method="get" action="#">
                                        <label><CakePHPBakeOpenTag= __('Search:') CakePHPBakeCloseTag><input type="search" class="form-control form-control-sm" placeholder="" name="search_term" aria-controls="sorting-table" value="<CakePHPBakeOpenTag= $search_term CakePHPBakeCloseTag>"></label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <table class="table mb-0 dataTable table-hover">
                                    <thead>
                                    <tr>
                                        <?php foreach ($fields as $field): ?>

                                        <CakePHPBakeOpenTagphp
                                        if ($this->request->getQuery('sort') == '<?= $field ?>') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        CakePHPBakeCloseTag>
                                        <th scope="col" class="<CakePHPBakeOpenTag= $sort_class CakePHPBakeCloseTag> sorting"><CakePHPBakeOpenTag= $this->Paginator->sort('<?= $field ?>') CakePHPBakeCloseTag></th>
                                        <?php endforeach; ?>
                                        <th scope="col" class="actions"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <CakePHPBakeOpenTagphp foreach ($<?= $pluralVar ?> as $key => $<?= $singularVar ?>): CakePHPBakeCloseTag>
                                    <tr class="entity-row-<CakePHPBakeOpenTag= $key CakePHPBakeCloseTag>">
                                        <?php        foreach ($fields as $field) {
                                        $isKey = false;
                                        if (!empty($associations['BelongsTo'])) {
                                        foreach ($associations['BelongsTo'] as $alias => $details) {
                                        if ($field === $details['foreignKey']) {
                                        $isKey = true;
                                        ?>
                                        <td><CakePHPBakeOpenTag= $<?= $singularVar ?>->has('<?= $details['property'] ?>') ? $this->Html->link($<?= $singularVar ?>-><?= $details['property'] ?>-><?= $details['displayField'] ?>, ['controller' => '<?= $details['controller'] ?>', 'action' => 'view', $<?= $singularVar ?>-><?= $details['property'] ?>-><?= $details['primaryKey'][0] ?>]) : '' CakePHPBakeCloseTag></td>
                                        <?php
                                        break;
                                        }
                                        }
                                        }
                                        if ($isKey !== true) {
                                        if (!in_array($schema->columnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
                                        ?>
                                        <td><CakePHPBakeOpenTag= h($<?= $singularVar ?>-><?= $field ?>) CakePHPBakeCloseTag></td>
                                        <?php
                                        } else {
                                        ?>
                                        <td><CakePHPBakeOpenTag= $this->Number->format($<?= $singularVar ?>-><?= $field ?>) CakePHPBakeCloseTag></td>
                                        <?php
                                        }
                                        }
                                        }

                                        $pk = '$' . $singularVar . '->' . $primaryKey[0];
                                        ?>
                                        <td class="actions">
                                            <CakePHPBakeOpenTag= $this->Html->link(__('View'), ['action' => 'view', <?= $pk ?>], ['class' => 'btn btn-primary btn-square btn-sm']) CakePHPBakeCloseTag>
                                            <CakePHPBakeOpenTag= $this->Html->link(__('Edit'), ['action' => 'edit', <?= $pk ?>], ['class' => 'btn btn-info btn-square btn-sm']) CakePHPBakeCloseTag>
                                            <btn class="delete-entity btn btn-sm btn-square btn-danger" data-key="<CakePHPBakeOpenTag= $key CakePHPBakeCloseTag>" data-csrf="<CakePHPBakeOpenTag= $this->request->getParam('_csrfToken') CakePHPBakeCloseTag>" data-id="<CakePHPBakeOpenTag= <?= $pk ?> CakePHPBakeCloseTag>" data-delete-url="<CakePHPBakeOpenTag= $this->Url->build(['action' => 'delete', '_ext' => 'json']); CakePHPBakeCloseTag>"><CakePHPBakeOpenTag= __('Delete') CakePHPBakeCloseTag></btn>
                                        </td>
                                    </tr>
                                    <CakePHPBakeOpenTagphp endforeach; CakePHPBakeCloseTag>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" role="status" aria-live="polite">
                                    <CakePHPBakeOpenTag= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) CakePHPBakeCloseTag>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <CakePHPBakeOpenTag= $this->Paginator->first() CakePHPBakeCloseTag>
                                        <CakePHPBakeOpenTag= $this->Paginator->prev() CakePHPBakeCloseTag>
                                        <CakePHPBakeOpenTag= $this->Paginator->numbers() CakePHPBakeCloseTag>
                                        <CakePHPBakeOpenTag= $this->Paginator->next() CakePHPBakeCloseTag>
                                        <CakePHPBakeOpenTag= $this->Paginator->last() CakePHPBakeCloseTag>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <CakePHPBakeOpenTagphp else : CakePHPBakeCloseTag>
                    <h3 class="text-center"><CakePHPBakeOpenTag= sprintf(__('There are no records yet. <a href="%s">Click here to add one.</a>'), $this->Url->build(['controller' => '<?= $pluralHumanName ?>', 'action' => 'add'])) CakePHPBakeCloseTag></h3>
                <CakePHPBakeOpenTagphp endif; CakePHPBakeCloseTag>
            </div>
        </div>
    </div>
</div>